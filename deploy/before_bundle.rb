# link sites directory
if !File.exist?(config.shared_path + "/sites/default/settings.php") then
  run "mkdir -p #{config.shared_path}/sites/default && cp -a #{config.release_path}/sites/default/settings.php #{config.shared_path}/sites/default/settings.php"
end
run "rm -Rf #{config.release_path}/sites/default/settings.php && ln -s #{config.shared_path}/sites/default/settings.php ./sites/default/settings.php"
